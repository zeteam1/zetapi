const jwt = require('jsonwebtoken');
const config = require('../config/config');
const errorCodes = require('../config/errors');
const {checkAndDisplayError, displayError} = require('./errorHandler');


let jwtSign = function (payload, expireTime) {
    if (payload) {
        let expire = typeof expireTime  !== 'undefined' ?  expireTime  : config.JWT_EXPIRE;     
        return jwt.sign(payload, config.SECRET_KEY, {
            expiresIn: expire 
        });
    }
    return null;    
}

let jwtDetector = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];  
    if (token) {
      jwt.verify(token, config.SECRET_KEY, function(err, decoded) {      
        if (!checkAndDisplayError(err, 500, errorCodes.SERVER_ERROR, res)){
          req.jwtDecoded = decoded;
          next();
        }
      });  
    }else{
        displayError(403, errorCodes.PERMISSION_DENIED, res); 
    }
    next();
}

module.exports = {
    jwtSign,
    jwtDetector
}