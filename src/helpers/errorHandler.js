const {log} = require('../helpers/winston');

let checkAndDisplayError = function (err, status, errorCode, res) {
    if (err) {
        res.status(status).json(errorCode); 
        log.error(err);
        log.error(new Error().stack);
        return true;
    } 
    return false;
}

let displayError = function (status, errorCode, res) {    
    res.status(status).json(errorCode); 
    log.error(errorCode);
    log.error(new Error().stack);
}

exports.checkAndDisplayError = checkAndDisplayError;
exports.displayError = displayError;