const {log} = require('./winston');
const errorCodes = require('../config/errors');
const userModel = require('../models/userModel');

let registerValidate = function (data, res, callback) {
    let errors = [];
    checkInput(data, errors, function(data, errors, iterator) {
        if (errors.length) {
            res.status(400).json(errors);
            log.error("Registration failed" + JSON.stringify(errors));
        }
        else {
            callback(data);
        }
    })
}

let checkInput = function(data, errors, callback) {
    if (Object.keys(data).length !== 0 && data.username && data.password && data.rePassword && data.email) {
        let callbacks = [checkPassword, checkEmailFormat, checkEmailUnique, checkUsernameUnique, callback];
        let iterator = makeIterator(callbacks);
        iterator.next().value(data, errors, iterator);
    } else {
        errors.push(errorCodes.MISSING_DATA);
        callback(data, errors);
    }
}

let checkPassword = function(data, errors, iterator) {
    if (data.password !== data.rePassword) {
        errors.push(errorCodes.PASSWORDS_MISSMATCH);
    }
    iterator.next().value(data, errors, iterator);
}

let checkEmailFormat = function(data, errors, iterator) {
    let regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!regex.test(data.email)) {
        errors.push(errorCodes.INVALID_EMAIL);
    }
    iterator.next().value(data, errors, iterator);
}

let checkEmailUnique = function(data, errors, iterator) {
    userModel.emailExists(data.email, function(count) {
        if (count){
            errors.push(errorCodes.EMAIL_EXISTS);
        }
        iterator.next().value(data, errors, iterator);
    })
}

let checkUsernameUnique = function(data, errors, iterator) {
    userModel.usernameExists(data.username, function(count) {
        if (count){
            errors.push(errorCodes.USERNAME_EXISTS);
        }
        iterator.next().value(data, errors, iterator);
    })
}

let makeIterator = function(array) {
    var nextIndex = 0;
    return {
       next: function() {
           return nextIndex < array.length ? {value: array[nextIndex++], done: false} : {done: true};
       }
    };
}

exports.registerValidate = registerValidate;