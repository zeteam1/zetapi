# Zet API

A basic api for general user, the main idea of this is to create the basic functionalities that are generally used in a project and use it as a starting point

## Goals

* Authenticate system with JWT
* Basic user management
* Authorize system (admin levels and such)
* Error Handler
* Email system 
* Basic article management (interconected with users)


## Authors

* **Seviciu Cosmin**
* **Pantea Cornelius**

