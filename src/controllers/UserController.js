const userModel = require('../models/userModel');
const jwt = require('jsonwebtoken');
const {log} = require('../helpers/winston');
const {registerValidate} = require('../helpers/validator');
const errorCodes = require('../config/errors');
const bcrypt = require('bcrypt');
const config = require('../config/config');
const {checkAndDisplayError, displayError} = require('../helpers/errorHandler');

