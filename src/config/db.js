let mysql = require('mysql');
let Sequelize = require('sequelize');
let config = require('./config');
const Op = Sequelize.Op;
let { log } = require('../helpers/winston');

let connection = new Sequelize(config.DATABASE, config.USER, config.PASSWORD, {
    host: config.HOST,
    dialect: 'mysql',  
    operatorsAliases: Op,
    logging: true,
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
  });

  connection.authenticate()
  .then(() => {
    log.info('Connection has been established successfully.');
  })
  .catch(err => {
    log.error(err);
  });

module.exports = {
    Sequelize : Sequelize,
    connection : connection
}


