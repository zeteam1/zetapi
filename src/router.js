const app = require('express').Router();
const config = require('./config/config');
const authController = require('./controllers/authController');
const {jwtDetector} = require("./helpers/jwtManager");




app.route('/').post((req, res) => {
    res.status(200).json({ message: 'Connected!' });
});

app.route('/login').post(authController.login);
app.route('/register').post(authController.register);

app.use(jwtDetector); // all routes above this do not require JWT

module.exports = app;