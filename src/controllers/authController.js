const userModel = require('../models/userModel');
const {jwtSign} = require('../helpers/jwtManager');
const {log} = require('../helpers/winston');
const {registerValidate} = require('../helpers/validator');
const errorCodes = require('../config/errors');
const bcrypt = require('bcrypt');
const config = require('../config/config');
const {checkAndDisplayError, displayError} = require('../helpers/errorHandler');

let login = function(req, res) {
    
    let body = req.body;
    let user = null;

    let init = function () {
        if (Object.keys(body).length !== 0 && body.username && body.password) {
            userModel.getUserByUsername(body.username, handleUser);
        } else{
            displayError(500, errorCodes.MISSING_DATA, res); 
        }
    }

    let handleUser = function (userResource) {
        user = userResource.dataValues;
        if (user) {         
            bcrypt.compare(body.password, user.password, processBcrypt);
        }else{
            displayError(500, errorCodes.INVALID_USERNAME_OR_PASSWORD, res);
        }        
    }

    let processBcrypt = function (err, match) {
        if(!checkAndDisplayError(err, 500, errorCodes.SERVER_ERROR, res) && user) {
            if(match) {
                res.status(200);
                let token = jwtSign({ email: user.email, username: user.username, id: user.id }, 1440);
                res.json({ token: token});
            }else{
                displayError(500, errorCodes.INVALID_USERNAME_OR_PASSWORD, res);    
            }
        }
    }   

    init();
}

let register = function (req, res) {
    let data = req.body;

    let init = function () {
        registerValidate(data, res, bcryptData);    
    }

    let bcryptData = function (data) {
        bcrypt.genSalt(config.SALT_ROUNDS, (err, salt) => {
            if(!checkAndDisplayError(err, 500, errorCodes.SERVER_ERROR, res)) {
                bcrypt.hash(data.password, salt, createUser);
            }
        });
    }

    let createUser = function (err, hash) {
        if(!checkAndDisplayError(err, 500, errorCodes.SERVER_ERROR, res)) {
            userModel.createUser({                    
                username: data.username,
                lastName: "",
                firstName: "",
                email : data.email,
                password : hash
            }, (user) => {
                if (user){
                    res.status(200).json({message : "user created"});
                } else{
                    displayError(500, errorCodes.REGISTRATION_FAILED, res);   
                }                    
            });
        }
    }
    
    init();
}

exports.login = login;
exports.register = register;