let {connection, Sequelize} = require('../config/db');

let User = connection.define('user', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
    username: Sequelize.STRING,
    firstname: Sequelize.STRING,
    lastname: Sequelize.STRING,
    email: {
        type : Sequelize.STRING,
        unique : true
    },
    password: Sequelize.STRING,
    deleted: {
        type: Sequelize.BOOLEAN,
        defaultValue: 0
    }
});


/** 
 * @param {Object} obj 
 * @param {Function} callback 
 */
let createUser = function (obj, callback) {
    connection.sync().then(() => {
        User.create({
            username: obj.username,
            firstname: obj.firstname,
            lastname: obj.lastname,
            email: obj.email,
            password: obj.password,
        }).then(callback);
    });
}

/**
 * @param {String} username 
 * @param {String} password
 * @param {Function} callback 
 */
let getUserByUsername = function (username, callback) {
    User.findOne({
        where: {
            username : username,
            deleted : 0
        }
    }).then(callback);
}

let usernameExists = function (username, callback) {
    User.count({
        where:{
            username: username
        }
    }).then(count => {
        callback(count);
    })
}

let emailExists = function (email, callback) {
    let count = User.count({
        where:{
            email: email
        }
    }).then(count => {
        callback(count);
    })
}

module.exports = {
    createUser, getUserByUsername, emailExists, usernameExists
}


