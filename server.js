const express = require('express');
const parser = require('body-parser');
const db = require('./src/config/db');
const user = require('./src/models/userModel');
const router = require('./src/router');
const config = require('./src/config/config');
const { log } = require('./src/helpers/winston');

//Setup Express

let app = express(); 
app.use(parser.json());

app.use(parser.urlencoded({extended:true}));
app.set('port', process.env.PORT || config.PORT);

app.use('/', router);
// Create Server

let server = app.listen(app.get('port'), function () {
    log.info('Server listening on port ' + app.get('port'));
});


